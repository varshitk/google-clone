import React ,{createContext, useContext,useReducer} from "react";

export const StateContext = createContext(); //data Layer , reducer listens to change

export const StateProvider = ({reducer,initialState,children}) => (
    <StateContext.Provider value={useReducer(reducer,initialState)}>
        {children}
    </StateContext.Provider>

);

//hook which allows us to pull information from the data layer

export const useStateValue = () => useContext(StateContext);